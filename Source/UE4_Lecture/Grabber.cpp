// Fill out your copyright notice in the Description page of Project Settings.


#include "Grabber.h"

#include "Chaos/Private/kDOP.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	// Find ActorComponents
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();

	// Double Check
	if (PhysicsHandle == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("PhysicsHandleComponent is not on the actor"));
	}
	if (InputComponent)
	{
		// KeyBinding
		InputComponent->BindAction(FName("Grab/Release"), IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction(FName("Grab/Release"), IE_Released, this, &UGrabber::Release);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("InputComponent is not on the actor"));
	}
}


void UGrabber::Grab()
{
	UE_LOG(LogTemp, Warning, TEXT("Grab Pressed"));
	// Ray Cast
	FHitResult HitResult = GetFirstObjectInReach();
	// if Hit
	if (HitResult.GetComponent())
	{
		// Grab
		PhysicsHandle->GrabComponentAtLocation(HitResult.GetComponent(), NAME_None, HitResult.GetActor()->GetActorLocation());
	}
}


void UGrabber::Release()
{
	UE_LOG(LogTemp, Warning, TEXT("Release Pressed"));
	// Release
	PhysicsHandle->ReleaseComponent();
}

FHitResult UGrabber::GetFirstObjectInReach() const
{
	FHitResult HitResult;
	FCollisionQueryParams TraceParameters(FName(""), false, GetOwner());
	GetWorld()->LineTraceSingleByObjectType(HitResult,
										GetOwner()->GetActorLocation(),
										LineTraceEnd(),
										ECollisionChannel::ECC_PhysicsBody,
										TraceParameters
										);
	return HitResult;
}

FVector UGrabber::LineTraceEnd() const
{
	FVector PlayerViewpointLocation;
	FRotator PlayerViewpointRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(PlayerViewpointLocation, PlayerViewpointRotation);
	return GetOwner()->GetActorLocation() + Reach * PlayerViewpointRotation.Vector();
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// if Grabbing sth
	if (PhysicsHandle->GrabbedComponent != nullptr)
	{
		// Set Location to Trace End
		PhysicsHandle->SetTargetLocation(LineTraceEnd());
	}
}


