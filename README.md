# UE4_Lecture

This repo stores all code and project files of lectures related to Computerization's UE4 lectures
Note that this repo is built using the Unreal Engine 5 beta edition.

## How to use
1.Clone this repository  
2.Right click the `.uproject` file  
3.Click `Generate Visual Studio Project Files`  
4.After compilation, open the `.uproject` file  
5.You are ready to go

## Lecture Contents
### Lecture 1 : UPROPERTY, UFUNCTION, Object Reference & Materials
### Lecutre 2 : Customized PawnActor, ActorComponents and KeyBindings
### Lecture 3 : Migrate to UE5, Grab/Release Physics Body